package com.somta.common.base;

import java.util.List;

public interface IBaseDao {

	
	<T> int add(T t);
	
	int deleteById(Object id);
	
	<T> int update(T t);
	
	<T> T queryById(Object id);
	
	<T> List<T> queryByList(Object object);

	
}
