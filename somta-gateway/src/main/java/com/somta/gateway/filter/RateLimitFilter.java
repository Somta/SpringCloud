package com.somta.gateway.filter;

import com.google.common.util.concurrent.RateLimiter;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import com.somta.gateway.exception.RateLimitException;
import org.springframework.stereotype.Component;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SERVLET_DETECTION_FILTER_ORDER;

/**
 * @Description: 通过“令牌桶”算法，完成对请求的限流
 * @Date: 2019/7/9
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@Component
public class RateLimitFilter extends ZuulFilter{

    //这里使用谷歌开源的令牌桶算法，每秒限制100个令牌
    private static  final RateLimiter rateLimit = RateLimiter.create(100);

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        //限流的优先级应该是最高的
        return SERVLET_DETECTION_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //没有拿到令牌
        if (!rateLimit.tryAcquire()) {
            throw new RateLimitException();
        }
        return null;
    }
}
