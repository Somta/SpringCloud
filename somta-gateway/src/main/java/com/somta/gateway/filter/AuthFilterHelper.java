package com.somta.gateway.filter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Date: 2019/7/9
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@Component
public class AuthFilterHelper {

    @Autowired
    private Environment env;

    private List<String> ignoreFilterPathPrefixPatterns = new ArrayList<>();

    @PostConstruct
    public void init() {
        String ignoreFilterPathPrefixPattern = env.getProperty("ignoreFilterPathPrefixPattern");
        if (StringUtils.isNotBlank(ignoreFilterPathPrefixPattern)) {
            String [] pps = StringUtils.split(ignoreFilterPathPrefixPattern, ",");
            for (String pp : pps) {
                if (StringUtils.isNotBlank(pp)) {
                    ignoreFilterPathPrefixPatterns.add(pp.trim());
                }
            }
        }
    }

    public boolean ignoredAuthenticationPath(String path) {
        for (String ignorePath : ignoreFilterPathPrefixPatterns) {
            //System.out.println(ignorePath);
            if (path.startsWith(ignorePath)) {
                return false;
            }
        }
        return true;
    }

}
