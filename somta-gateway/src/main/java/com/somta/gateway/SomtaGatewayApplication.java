package com.somta.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @Description:
 * @Date: 2019/7/5
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@SpringBootApplication
@EnableZuulProxy
public class SomtaGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SomtaGatewayApplication.class, args);
    }

}
