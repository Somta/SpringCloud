# SpringCloud

#### 介绍
以SpringBoot 2.1.5.RELEASE + Greenwich.SR1 搭建一套可以直接用于企业级开发的脚手架，让小白可以轻松学习，让企业可以直接使用

#### 软件架构

SpringBoot 2.1.5.RELEASE + Greenwich.SR1 + Nacos + 统一网关zuul + 服务降级熔断 + 链路跟踪 + docker + 事务实现

#### 修改优化
1、优化断网后SpringCloudBus的匿名队列销毁后无法自动创建
2、添加简单分布式事务处理

3、添加Nacos的注册服务发现与配置中心
