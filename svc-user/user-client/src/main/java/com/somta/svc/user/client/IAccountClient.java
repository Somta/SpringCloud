package com.somta.svc.user.client;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.common.base.result.ResponsePaginationDataResult;
import com.somta.svc.user.pojo.in.AccountIn;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 * @Date: 2019/6/27
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@FeignClient(name = "svc-user", fallback = IAccountClient.AccountSvcHystrixClientFallback.class)
public interface IAccountClient {


    @PostMapping("/account/updateAccountScore")
    ResponseDataResult updateAccountScore(@RequestBody AccountIn accountIn);

    @Component
    static class AccountSvcHystrixClientFallback implements IAccountClient {

        @Override
        public ResponseDataResult updateAccountScore(AccountIn accountIn) {
            return ResponseDataResult.setErrorResponseResult("调用svc-user服务updateAccountScore方法失败");
        }
    }

}
