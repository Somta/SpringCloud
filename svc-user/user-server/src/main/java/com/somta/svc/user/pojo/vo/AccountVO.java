package com.somta.svc.user.pojo.vo;


import com.somta.common.base.vo.PageVO;

/**
 * 
 * @Description: 账户信息VO实体类
 *
 * @Date:        2018-05-07
 * @Author:      husong
 * @Version:     1.0.0
 */
public class AccountVO extends PageVO {
	
	private String accountName;//   账号名称
	private String accountType;//   SUPER_ADMINISTRATOR:超级管理员， COMMON_ROLE：普通角色
	private Integer status;//   （0:删除,1:正常,2:禁用）默认值:1
	private String phone;//   手机号码
	private String email;//   邮箱
	private String orgId;//   组织ID
	
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
}

