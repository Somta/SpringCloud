package com.somta.svc.user.pojo.vo;


import com.somta.common.base.vo.PageVO;

/**
 * 
 * @Description: 组织信息VO实体类
 *
 * @Date:        2018-05-07
 * @Author:      husong
 * @Version:     1.0.0
 */
public class OrganizationVO extends PageVO {
	
	private Integer status; //（0:删除,1:正常,2:禁用）默认值:1
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
