package com.somta.svc.user.pojo;

import com.somta.common.base.BasePojo;

import java.io.Serializable;


/**
 * 
 * @Description: 组织机构实体类
 *
 * @Date:        2018-5-07
 * @Author:      husong
 * @Version:     1.0.0
 */
public class Organization extends BasePojo implements Serializable{

	private static final long serialVersionUID = -5468360063624400581L;
	
	private String orgId;  //组织ID
	private String orgCode;  //机构代码号
	private String orgName;  //组织名称
	private Integer orgType;  //机构类型
	private String orgAddress;  //机构地址
	private Integer status;  //（0:删除,1:正常,2:禁用）默认值:1
	private String remark;  //备注
	
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Integer getOrgType() {
		return orgType;
	}
	public void setOrgType(Integer orgType) {
		this.orgType = orgType;
	}
	public String getOrgAddress() {
		return orgAddress;
	}
	public void setOrgAddress(String orgAddress) {
		this.orgAddress = orgAddress;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
