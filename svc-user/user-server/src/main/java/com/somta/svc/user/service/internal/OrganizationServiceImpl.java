package com.somta.svc.user.service.internal;

import com.somta.common.base.BaseServiceImpl;
import com.somta.common.base.IBaseDao;
import com.somta.svc.user.dao.OrganizationDao;
import com.somta.svc.user.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationServiceImpl extends BaseServiceImpl implements IOrganizationService {

	
	@Autowired
	private OrganizationDao organizationDao;

	@Override
	public IBaseDao getDao() {
		return organizationDao;
	}
	
}
