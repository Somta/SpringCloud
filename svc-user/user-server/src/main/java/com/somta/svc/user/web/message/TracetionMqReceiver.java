package com.somta.svc.user.web.message;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;


/**
 * @Description: 主要用来接收
 * @Date: 2019/7/4
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@Component
public class TracetionMqReceiver {


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("messageQueue"),
            exchange = @Exchange("userExchange")
    ))
    public void processTopic(String message){
        System.out.println("接到消息了");
        System.out.println(message);
    }

}
