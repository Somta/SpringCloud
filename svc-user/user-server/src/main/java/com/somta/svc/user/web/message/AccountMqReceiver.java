package com.somta.svc.user.web.message;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Description: 主要用来接收
 * @Date: 2019/7/4
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@Component
public class AccountMqReceiver {

    /**
     * 接收MQ消息后，处理账户积分变化
     */
    //1、需要手动创建  @RabbitListener(queues = "scoreQueue")
    //2、如果RabbitMQ中无该队列，自动创建队列   @RabbitListener(queuesToDeclare = @Queue("scoreQueue"))
    //3、设置队列与Exchange进行绑定，如果不存在队列和Exchange，会自动创建
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("scoreQueue"),
            exchange = @Exchange("userExchange")
    ))
    public void processScore(String message){
        System.out.println(message);
    }

    /**
     * 根据key来区分不同的消息类型，指定类型的消息让指定的消费者消费
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange("userExchange"),
            key = "account",
            value = @Queue("keyQueue")
    ))
    public void processByKey(String message){
        System.out.println(message);
    }

}
