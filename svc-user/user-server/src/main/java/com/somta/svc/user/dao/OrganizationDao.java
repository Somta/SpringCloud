package com.somta.svc.user.dao;


import com.somta.common.base.IBaseDao;

/**
 * 
 * @Description: 平台组织 Dao
 *
 * @Date:        2018-05-07
 * @Author:      husong
 * @Version:     1.0.0
 */
public interface OrganizationDao extends IBaseDao {
	
	
}
