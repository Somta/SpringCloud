package com.somta.svc.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Description: @EnableDiscoveryClient 该注解是将服务注册到注册中心
 * @Date: 2019/6/19
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SvcUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(SvcUserApplication.class, args);
    }

}
