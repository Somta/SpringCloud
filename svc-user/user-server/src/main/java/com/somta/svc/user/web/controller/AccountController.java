package com.somta.svc.user.web.controller;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.svc.user.pojo.Account;
import com.somta.svc.user.pojo.in.AccountIn;
import com.somta.svc.user.service.IAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Date: 2019/6/27
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private IAccountService accountService;

    /**
     * 修改用户积分
     * @param accountIn
     * @return
     */
    @PostMapping("/updateAccountScore")
    public ResponseDataResult updateAccountScore(@RequestBody AccountIn accountIn){
        log.info("start update score");
        try {
            Account account = new Account();
            account.setId(accountIn.getId());
            account.setScore(accountIn.getScore());
            accountService.update(account);
        } catch (Exception e) {
            log.error("修改用户积分异常");
            //1、通过异常让blog服务的方法回滚
            //throw new RuntimeException();
            //2、返回错误信息，在blog服务中通过TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();手动回滚
            return ResponseDataResult.setErrorResponseResult("修改用户积分异常");
        }
        return ResponseDataResult.setResponseResult();
    }

}
