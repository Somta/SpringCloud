/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : svc-user

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-06-28 10:49:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fw_account
-- ----------------------------
DROP TABLE IF EXISTS `fw_account`;
CREATE TABLE `fw_account` (
  `id` varchar(40) NOT NULL,
  `account_name` varchar(40) DEFAULT NULL COMMENT '账号名称',
  `nick_name` varchar(40) DEFAULT NULL COMMENT '昵称',
  `secret` varchar(64) DEFAULT NULL COMMENT '密码',
  `account_type` varchar(40) DEFAULT NULL COMMENT 'SUPER_ADMINISTRATOR:超级管理员， ORG_ADMIN：机构管理员， COMMON_ADMIN：普通管理员',
  `status` int(2) DEFAULT '1' COMMENT '（0:删除,1:正常,2:禁用）默认值:1',
  `score` int(10) DEFAULT NULL COMMENT '积分',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `id_card` varchar(20) DEFAULT NULL COMMENT '省份证号',
  `org_id` varchar(40) DEFAULT NULL COMMENT '组织ID',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(40) DEFAULT NULL COMMENT '创建人',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(40) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fw_account
-- ----------------------------
INSERT INTO `fw_account` VALUES ('22', '张三', '张三', null, null, '1', '10', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for fw_organization
-- ----------------------------
DROP TABLE IF EXISTS `fw_organization`;
CREATE TABLE `fw_organization` (
  `id` varchar(40) NOT NULL COMMENT 'ID',
  `org_id` varchar(40) NOT NULL COMMENT '组织ID',
  `org_code` varchar(20) DEFAULT NULL COMMENT '机构代码号',
  `org_name` varchar(255) DEFAULT NULL COMMENT '组织名称',
  `org_type` int(2) DEFAULT NULL COMMENT '机构类型',
  `org_address` varchar(100) DEFAULT NULL COMMENT '机构地址',
  `status` int(2) DEFAULT '1' COMMENT '（0:删除,1:正常,2:禁用）默认值:1',
  `account_num` int(11) DEFAULT NULL COMMENT '组织总账号数',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(40) DEFAULT NULL COMMENT '创建人',
  `updated_at` datetime DEFAULT NULL COMMENT '修改时间',
  `updated_by` varchar(40) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of fw_organization
-- ----------------------------
