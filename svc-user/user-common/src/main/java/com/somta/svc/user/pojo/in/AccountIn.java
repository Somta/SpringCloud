package com.somta.svc.user.pojo.in;

import java.io.Serializable;

/**
 * @Description:
 * @Date: 2019/6/27
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
public class AccountIn implements Serializable {

    private String id;//
    private Integer score;//  积分

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
