package com.somta.svc.user.pojo.out;

/**
 * @Description:
 * @Date: 2019/6/27
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
public class AccountOut {

    private String id;//
    private String accountName;//   账号名称
    private String nickName;//   昵称
    private String accountType;//   SUPER_ADMINISTRATOR:超级管理员， COMMON_ADMIN：普通管理员
    private Integer status;//   （0:删除,1:正常,2:禁用）默认值:1
    private Integer score;//  积分
    private String phone;//   手机号码
    private String email;//   邮箱
    private String idCard;//   省份证号
    private String orgId;//   组织ID

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
