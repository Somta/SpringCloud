# SpringCloud

在nacos的配置列表新增一个名称svc-blog-config-dev.yml文件，内容如下，供bootstrap.properties使用

server:
  port: 9050
  servlet:
    context-path: /blog
    
spring:
  application:
    name: svc-blog
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848

  datasource:
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://127.0.0.1:3306/svc-blog?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
      username: root
      password: 123456

  rabbitmq:
    host: 139.217.135.138
    port: 5672
    username: guest
    password: guest

ele:
  name: somta