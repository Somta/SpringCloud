/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : svc-blog

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-06-28 10:50:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_blog
-- ----------------------------
DROP TABLE IF EXISTS `t_blog`;
CREATE TABLE `t_blog` (
  `id` varchar(36) NOT NULL,
  `blog_title` varchar(80) DEFAULT NULL COMMENT '博文标题',
  `author` varchar(12) DEFAULT NULL COMMENT '博文作者',
  `blog_summary` varchar(500) DEFAULT NULL COMMENT '博文简介',
  `blog_type` int(2) DEFAULT NULL COMMENT '博文类型 1：原创 2：转载',
  `blog_top` int(2) DEFAULT '0' COMMENT '是否置顶   0:不置顶   1:置顶   默认值: 0',
  `blog_status` int(2) DEFAULT '1' COMMENT '博文状态 （0:删除,1:正常）默认值:1',
  `markdown` text COMMENT '博文markdown内容',
  `blog_content` text COMMENT '博文内容',
  `subject_id` varchar(36) DEFAULT NULL COMMENT '专题ID',
  `read_num` int(8) DEFAULT '0' COMMENT '阅读数量',
  `tags` varchar(40) DEFAULT NULL COMMENT '文章标签，多个标签用分号隔开',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_blog
-- ----------------------------
INSERT INTO `t_blog` VALUES ('2', '测试标题', '张三', null, null, '0', '1', null, null, null, '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for t_subject
-- ----------------------------
DROP TABLE IF EXISTS `t_subject`;
CREATE TABLE `t_subject` (
  `id` varchar(36) NOT NULL,
  `subject_name` varchar(20) DEFAULT NULL,
  `subject_status` int(2) DEFAULT '1' COMMENT '专题状态（0:删除,1:正常）默认值:1',
  `blog_num` int(4) DEFAULT NULL COMMENT '博文数量',
  `blog_read_num` int(8) DEFAULT NULL COMMENT '专题博文阅读数量',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_subject
-- ----------------------------
