package com.somta.svc.blog.service.internal;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.svc.blog.pojo.Blog;
import com.somta.svc.blog.pojo.in.BlogIn;
import com.somta.svc.blog.service.IBlogService;
import com.somta.svc.blog.service.ITransactionService;
import com.somta.svc.user.client.IAccountClient;
import com.somta.svc.user.pojo.in.AccountIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * @Description:
 * @Date: 2019/8/20
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@Service
public class TransactionServiceImpl implements ITransactionService {

    private static final Logger log = LoggerFactory.getLogger(BlogServiceImpl.class);

    @Autowired
    private IBlogService blogService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private IAccountClient accountClient;

    @Transactional
    @Override
    public ResponseDataResult addBlogAndSendMsg(BlogIn blogIn) {
        Blog blog = new Blog();
        blog.setId(blogIn.getId());
        blog.setBlogTitle(blogIn.getBlogTitle());
        blog.setAuthor(blogIn.getAuthor());
        try {
            blogService.add(blog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (blogIn.getBlogTitle().contains("exception1")) {
            throw new RuntimeException("exception1");
        }
        rabbitTemplate.convertAndSend("messageQueue","update scope");
        if (blogIn.getBlogTitle().contains("exception2")) {
            throw new RuntimeException("exception2");
        }
        return ResponseDataResult.setResponseResult();
    }

    @Transactional
    @Override
    public ResponseDataResult addBlogAndRollback(BlogIn blogIn){
        Blog blog = new Blog();
        blog.setId(blogIn.getId());
        blog.setBlogTitle(blogIn.getBlogTitle());
        blog.setAuthor(blogIn.getAuthor());
        try {
            blogService.add(blog);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AccountIn accountIn = new AccountIn();
        ResponseDataResult rst = accountClient.updateAccountScore(accountIn);
        if (!rst.isSuccess()){
            //account 服务异常  手动回滚事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return ResponseDataResult.setResponseResult();
    }

}
