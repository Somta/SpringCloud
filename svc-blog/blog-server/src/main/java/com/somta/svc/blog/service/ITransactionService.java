package com.somta.svc.blog.service;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.svc.blog.pojo.in.BlogIn;

/**
 * @Description:
 * @Date: 2019/8/20
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
public interface ITransactionService {

    ResponseDataResult addBlogAndSendMsg(BlogIn blogIn);

    ResponseDataResult addBlogAndRollback(BlogIn blogIn);
}
