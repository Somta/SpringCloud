package com.somta.svc.blog.web.controller;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.svc.blog.pojo.in.BlogIn;
import com.somta.svc.blog.service.ITransactionService;
import com.somta.svc.blog.service.internal.BlogServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Date: 2019/8/20
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@RestController
@RequestMapping("/blog/transaction")
public class TransactionController {

    private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private ITransactionService transactionService;

    /**
     * 新增博文，并且发送消息，测试事务
     * @param blogIn
     * @return
     */
    @PostMapping("/addBlogAndSendMsg")
    public ResponseDataResult addBlogAndSendMsg(@RequestBody BlogIn blogIn) {
        try {
            return transactionService.addBlogAndSendMsg(blogIn);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseDataResult.setErrorResponseResult("新增博客异常");
        }
    }

    /**
     * 新增博文，账户服务异常，回滚博文服务
     * @param blogIn
     * @return
     */
    @PostMapping("/addBlogAndRollback")
    public ResponseDataResult addBlogAndRollback(@RequestBody BlogIn blogIn) {
        try {
            return transactionService.addBlogAndRollback(blogIn);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseDataResult.setErrorResponseResult("新增博客异常");
        }
    }

}
