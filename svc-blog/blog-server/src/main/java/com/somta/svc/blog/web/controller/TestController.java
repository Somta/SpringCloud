package com.somta.svc.blog.web.controller;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.svc.blog.pojo.Blog;
import com.somta.svc.blog.pojo.in.BlogIn;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:
 * @Date: 2019/9/17
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@RefreshScope
@RestController
@RequestMapping("/pub/test")
public class TestController {

    @Value("${ele.name}")
    private String name;

    /**
     * 获取配置中心配置
     * @return
     */
    @GetMapping("/getEleName")
    public String updateBlog(){
        return this.name;
    }

}
