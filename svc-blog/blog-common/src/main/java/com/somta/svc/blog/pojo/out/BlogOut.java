package com.somta.svc.blog.pojo.out;

/**
 * @Description:
 * @Date: 2019/7/5
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
public class BlogOut {

    private String id;//
    private String blogTitle;//   博文标题
    private String author;//  博文作者
    private String blogSummary;//   博文简介
    private Integer blogType;//   博文类型 1：原创 2：转载
    private Integer blogTop;//  是否置顶   0:不置顶   1:置顶   默认值: 0
    private Integer blogStatus;//   博文状态 （0:删除,1:未发布 2:已发布）默认值:1
    private String markdown;//  博文markdown内容
    private String blogContent;//   博文HTML内容
    private String subjectId;//   专题ID
    private Integer readNum;//   阅读数量
    private String tags; //文章标签，多个标签用分号隔开

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBlogSummary() {
        return blogSummary;
    }

    public void setBlogSummary(String blogSummary) {
        this.blogSummary = blogSummary;
    }

    public Integer getBlogType() {
        return blogType;
    }

    public void setBlogType(Integer blogType) {
        this.blogType = blogType;
    }

    public Integer getBlogTop() {
        return blogTop;
    }

    public void setBlogTop(Integer blogTop) {
        this.blogTop = blogTop;
    }

    public Integer getBlogStatus() {
        return blogStatus;
    }

    public void setBlogStatus(Integer blogStatus) {
        this.blogStatus = blogStatus;
    }

    public String getMarkdown() {
        return markdown;
    }

    public void setMarkdown(String markdown) {
        this.markdown = markdown;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getReadNum() {
        return readNum;
    }

    public void setReadNum(Integer readNum) {
        this.readNum = readNum;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

}
