package com.somta.svc.blog.client;

import com.somta.common.base.result.ResponseDataResult;
import com.somta.common.base.result.ResponsePaginationDataResult;
import com.somta.svc.blog.pojo.in.BlogIn;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 * @Date: 2019/6/27
 * @Author: 明天的地平线
 * @Blog: https://somta.com.cn/index
 * @Version: 1.0.0
 */
@FeignClient(name = "svc-blog", fallback = IBlogClient.BlogSvcHystrixClientFallback.class)
public interface IBlogClient {

    @PostMapping("/blog/addBlog")
    ResponseDataResult addBlog(@RequestBody BlogIn blogIn);

    @PostMapping("/blog/queryBlogList")
    ResponsePaginationDataResult queryBlogList();

    @Component
    static class BlogSvcHystrixClientFallback implements IBlogClient {
        @Override
        public ResponseDataResult addBlog(BlogIn blogIn) {
            System.out.println("调用服务失败");
            return ResponseDataResult.setErrorResponseResult("调用svc-blog服务addBlog方法失败");
        }

        @Override
        public ResponsePaginationDataResult queryBlogList() {
            System.out.println("调用服务失败");
            return ResponsePaginationDataResult.setErrorResponseResult("调用svc-blog服务queryBlogList方法失败");
        }
    }


}